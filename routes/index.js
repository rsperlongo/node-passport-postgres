const express = require('express');
const router = express.Router();

const classroomController = require('../controllers').classroom;
const studentController = require('../controllers').student;
const lecturerController = require('../controllers').lecturer;
const courseController = require('../controllers').course;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* Classroom Router */
router.get('/api/classroom', function(req, res) {
  classroomController.list
});
router.get('/api/classroom/:id', function(req, res) {
  classroomController.getById
});
router.post('/api/classroom', function(req, res) {
  classroomController.add
});
router.put('/api/classroom/:id', function(req, res) {
  classroomController.update
});
router.delete('/api/classroom/:id', function(req, res) {
  classroomController.delete
});

/* Student Router */
router.get('/api/student', function(req, res) {
  studentController.list
});
router.get('/api/student/:id', function(req, res) {
  studentController.getById
});
router.post('/api/student', function(req, res) {
  studentController.add
});
router.put('/api/student/:id', function(req, res) {
  studentController.update
});
router.delete('/api/student/:id', function(req, res) {
  studentController.delete
});

/* Lecturer Router */
router.get('/api/lecturer', function(req, res) {
  lecturerController.list
});
router.get('/api/lecturer/:id', function(req, res) {
  lecturerController.getById
});
router.post('/api/lecturer', function(req, res) {
  lecturerController.add
});
router.put('/api/lecturer/:id', function(req, res) {
  lecturerController.update
});
router.delete('/api/lecturer/:id', function(req, res) {
  lecturerController.delete
});

/* Course Router */
router.get('/api/course', function(req, res) {
  courseController.list
});
router.get('/api/course/:id', function(req, res) {
  courseController.list
});
router.post('/api/course', function(req, res) {
  courseController.list
});
router.put('/api/course/:id', function(req, res) {
  courseController.update
});
router.delete('/api/course/:id', function(req, res) {
  courseController.delete
});

/* Advance Router */
router.post('/api/student/add_course', function(req, res) {
  studentController.addCourse
});
router.post('/api/classroom/add_with_students', function(req, res) {
  classroomController.addWithStudents
});
router.post('/api/lecturer/add_with_course', function(req, res) {
  lecturerController.addWithCourse
});

module.exports = router;