const classroom = require('./clasroom');
const student = require('./student');
const lecturer = require('./lecture');
const course = require('./course');

module.exports = {
  classroom,
  student,
  lecturer,
  course,
};